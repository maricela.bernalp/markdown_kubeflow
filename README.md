<!-- Headings -->
# Migración de trabajos de Apache Spark ML a Spark + Tensorflow en Kubeflow

 
### Migración en base a una combinación de Spark y Tensorflow. Usando el proyecto de código abierto Kubeflow.

![Markdown Logo](https://cloud.google.com/solutions/images/architecture-for-mlops-using-tfx-kubeflow-pipelines-and-cloud-build-6-ci-cd-kubeflow.svg?hl=es)


>¿Qué es Kubeflow?

Es una plataforma ML de código abierto dedicada a realizar implementaciones de flujos de trabajo de aprendizaje automático (ML) en Kubernetes simples, portátiles y escalables. Kubeflow Pipelines es parte de la plataforma Kubeflow que permite la composición y ejecución de flujos de trabajo reproducibles en Kubeflow, integrados con la experimentación y las experiencias basadas en portátiles. 

Los servicios de  Kubeflow Pipelines en Kubernetes incluyen la tienda de metadatos alojada, el motor de orquestación basado en contenedores, el servidor de notebook y la interfaz de usuario para ayudar a los usuarios a desarrollar, ejecutar y administrar tuberías complejas de ML a escala. El SDK de Kubeflow Pipelines permite la creación y el intercambio de componentes y composición y de tuberías mediante programación.

![Markdown Logo](https://res.infoq.com/news/2020/03/google-ai-platform-pipelines/en/resources/1gcp-ai-platform-1584904836421.jpg)

>¿Qué es Tensorflow?

* Herramienta de aprendizaje automático
* Más opciones de aprendizaje deeo que dentro de os Spark ML
 